import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const API_URL = 'http://192.168.1.164:8125/api'
const AXIOS_CONFIG = {
    headers: {
        "Access-Control-Allow-Origin": "*"
    }
}

export default new Vuex.Store({
    state: {
        token: localStorage.getItem('token') || null,
        status: '',
        user: [],
        organisations: [],
        device_types: [],
        categories: []
    },
    mutations: {
        AUTH_REQUEST: (state) => {
            state.status = 'loading'
        },
        AUTH_SUCCESS: (state, token) => {
            state.status = 'success'
            state.token = token
        },
        AUTH_ERROR: (state) => {
            state.status = 'error'
        },
        DESTROY_STATE_TOKEN: (state) => {
            state.token = null
        },
        SET_USER: (state, user) => {
            state.user = user
        },
        SET_ORGANISATIONS: (state, organisations) => {
            state.organisations = organisations
        },
        SET_DEVICE_TYPES: (state, device_types) => {
            state.device_types = device_types;
        },
        SET_ORGANISATION_CATEGORIES: (state, categories) => {
            state.categories = categories;
        }
    },
    getters: {
        isAuthenticated: state => !!state.token,
        authStatus: state => state.status,
        getUser: state => state.user,
        getOrganisations: state => state.organisations,
        getDeviceTypes: state => state.device_types,
        getOrganisationCategories: state => state.categories
    },
    actions: {
        LOGIN: ({commit, dispatch}, user) => {
            return new Promise((resolve, reject) => {
                commit('AUTH_REQUEST')
                axios.post(`${API_URL}/login`, user, AXIOS_CONFIG)
                    .then(response => {
                        const token = response.data.token
                        localStorage.setItem('token', token)
                        commit('AUTH_SUCCESS', token)
                        dispatch('USER_REQUEST')
                        resolve(response)
                    })
                    .catch(error => {
                        commit('AUTH_ERROR', error)
                        localStorage.removeItem('token')
                        reject(error)
                    });
            })
        },
        REGISTRATION: (context, credentials) => {
            axios.post(`${API_URL}/register`, credentials, AXIOS_CONFIG)
                .then(response => {
                    console.log(response);
                })
                .catch(error => {
                    console.log(error);
                });
        },
        USER_REQUEST: (context) => {
            if(context.state.token){
                return new Promise((resolve, reject) => {
                axios.get(`${API_URL}/user`, {
                    'headers': { 'Authorization': `bearer ${context.state.token}` }
                })
                    .then(response => {
                        console.log(response.data.user)
                        context.commit('SET_USER', response.data.user)
                        context.commit('SET_ORGANISATIONS', response.data.organisations)
                        resolve(response)
                    })
                    .catch(error => {
                        context.dispatch('DESTROY_TOKEN')
                        reject(error)
                    });
                })
            }
        },
        DESTROY_TOKEN: (context) => {
            if(context.getters.isAuthenticated) {
                axios.post(`${API_URL}/logout`, [],{
                    'headers': { 'Authorization': `bearer ${context.state.token}` }
                })
                    .then(response => {
                        localStorage.removeItem('token')
                        context.commit('DESTROY_STATE_TOKEN')
                    })
                    .catch(error => {
                        localStorage.removeItem('token')
                        context.commit('DESTROY_STATE_TOKEN')
                    });
            }
        },
        CREATE_ORGANISATION: ({state, dispatch}, data) => {
            return new Promise((resolve, reject) => {
                axios.post(`${API_URL}/organisation`, data,{
                    'headers': { 'Authorization': `bearer ${state.token}` }
                })
                    .then(response => {
                        resolve(response)
                        dispatch('USER_REQUEST')
                    })
                    .catch(error => {
                        reject(error)
                    });
            })
        },
        GET_DEVICE_TYPES: ({state, commit}) => {
            axios.get(`${API_URL}/devices/get-types`, {
                'headers': { 'Authorization': `bearer ${state.token}` }
            })
                .then(response => {
                    commit('SET_DEVICE_TYPES', response.data)
                })
                .catch(error => {
                    console.log(error)
                });
        },
        GET_ORGANISATION_CATEGORIES: ({state, commit}) => {
            axios.get(`${API_URL}/categories/get-categories`, {
                'headers': { 'Authorization': `bearer ${state.token}` }
            })
                .then(response => {
                    console.log(response.data)
                    commit('SET_ORGANISATION_CATEGORIES', response.data)
                })
                .catch(error => {
                    console.log(error)
                });
        },
        CREATE_DEVICE: ({state}, data) => {
            return new Promise((resolve, reject) => {
                axios.post(`${API_URL}/devices`, data,{
                    'headers': { 'Authorization': `bearer ${state.token}` }
                })
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    });
            })
        }
    }
})
