import Vue from 'vue'
import Router from 'vue-router'
import auth from './middleware/auth'
import Home from './views/Home.vue'
import Login from './views/Login.vue'
import Logout from './components/Logout.vue'
import Registration from './views/Registration.vue'
import CreateOrganisation from './views/organisation/CreateOrganisation.vue'
import DeviceRegistration from './views/devices/DeviceRegistration.vue'

Vue.use(Router)

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/registration',
            name: 'registration',
            component: Registration
        },
        {
            path: '/logout',
            name: 'logout',
            component: Logout,
            beforeEnter: auth
        },
        {
            path: '/',
            name: 'home',
            component: Home,
            beforeEnter: auth
        },
        {
            path: '/about',
            name: 'about',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
            beforeEnter: auth
        },
        {
            path: '/organisation/add',
            name: 'organisation-add',
            component: CreateOrganisation,
            beforeEnter: auth
        },
        {
            path: '/device/add',
            name: 'device-add',
            component: DeviceRegistration,
            beforeEnter: auth
        }
    ]
})
